// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;

import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DocumentComparator.ExtensionPoint;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStep;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.config.ModifiedWhitespaceBehaviour;
import com.deltaxml.cores9api.config.ResultReadabilityOptions;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.PresetPreservationMode;
import net.sf.saxon.s9api.Serializer;

public class DocumentComparatorSample {
  
  public static void main(String[] args) throws Exception {
        
    File input1= new File("input1.xml");
    File input2= new File("input2.xml");
    File resultFolder= new File("result");
    File dcResultFolder= new File(resultFolder, "DocumentComparator");
    dcResultFolder.mkdirs();
    
    DocumentComparator comparator = new DocumentComparator();
    
    comparator.setOutputProperty(Serializer.Property.INDENT, "yes");
    comparator.setOutputProperty(Serializer.Property.METHOD, "html");
    // the VERSION property below is for HTML5 and is only supported by Saxon 9.5
    comparator.setOutputProperty(Serializer.Property.VERSION, "5.0");
    
    // By default word by word comparison is performed on all elements
    ResultReadabilityOptions resultReadabilityOptions = comparator.getResultReadabilityOptions();
    resultReadabilityOptions.setModifiedWhitespaceBehaviour(ModifiedWhitespaceBehaviour.NORMALIZE);
    resultReadabilityOptions.setElementSplittingEnabled(false);
    
    // Ensure ignorable whitespace is not preserved
    LexicalPreservationConfig lexConfig= new LexicalPreservationConfig(PresetPreservationMode.ROUND_TRIP);
    lexConfig.setPreserveIgnorableWhitespace(false);
    comparator.setLexicalPreservationConfig(lexConfig);
    
    FilterStepHelper fsHelper= comparator.newFilterStepHelper();
    
    // Non Word By Word comparison
    resultReadabilityOptions.setOrphanedWordDetectionEnabled(false);
    FilterChain disableWBW= fsHelper.newFilterChain();
    FilterStep noWordByWord= fsHelper.newFilterStep(new File("disable-word-by-word.xsl"), "disable-word-by-word");
    disableWBW.addStep(noWordByWord);
    comparator.setExtensionPoint(ExtensionPoint.PRE_FLATTENING, disableWBW);
    noWordByWord.setEnabled(true);
    
    FilterChain convertToHTML= fsHelper.newSingleStepFilterChain(new File("convert-to-html.xsl"), "convert-to-html");
    comparator.setExtensionPoint(ExtensionPoint.OUTPUT_FINAL, convertToHTML);
    
    comparator.compare(input1, input2, new File(dcResultFolder, "non-wbw-result.html"));

    // Word by Word comparison
    noWordByWord.setEnabled(false);

    comparator.compare(input1, input2, new File(dcResultFolder, "wbw-result.html"));
    
    // Word by word with orphaned word processing
    noWordByWord.setEnabled(false);
    resultReadabilityOptions.setOrphanedWordDetectionEnabled(true);
    resultReadabilityOptions.setOrphanedWordLengthLimit(2);
    resultReadabilityOptions.setOrphanedWordMaxPercentage(20);
    
    comparator.compare(input1, input2, new File(dcResultFolder, "orphaned-words-result.html"));
  }
  
}
