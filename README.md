# Word By Word
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

*How to perform a detailed comparison on textual changes using filters included with XML Compare.*

This document describes how to run the sample. For concept details see: [Word by Word Text Comparison](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/word-by-word-text-comparison)

This sample code illustrates three methods for performing word by word text comparison. The first method uses the Pipelined Comparator, and specifies input and output filters with a DXP file (identified by the wbw configuration id). The remaining two methods use the Document Comparator, the first of these uses a DCP file (configuration-id dcp-wbw) to configure the comparator pipeline, whilst the second uses the Java API.

Once you have downloaded the files, if you're using any Java version of XML Compare and have Apache Ant installed, use the build script provided to run the complete sample. Simply type the following command. This will generate output for the three methods in the results directory.  


	ant run

If you don't have Ant installed, you can run the samples from a command line by issuing commands from the sample directory (ensuring that you use the correct directory and class path separators for your operating system).

## Pipelined Comparator (DXP)
To run just the Pipelined Comparator sample and produce the output files `non-wbw-result.html`, `wbw-result.html` and `orphaned-words-result.html` in the `result/PipelinedComparator` directory, type the following command:  


	ant run-dxp

When using any Java version of XML Compare, the commands to run only the Pipelined Comparator sample code are as follows. Replace x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`  


	java -jar ../../deltaxml-x.y.z.jar compare wbw input1.xml input2.xml non-wbw-result.html
	java -jar ../../deltaxml-x.y.z.jar compare wbw input1.xml input2.xml wbw-result.html word-by-word=true
	java -jar ../../deltaxml-x.y.z.jar compare wbw input1.xml input2.xml orphaned-words-result.html word-by-word=true orphaned-words=true

## Document Comparator (DCP)
To run just the Document Comparator DCP sample and produce the output files `non-wbw-result.html`, `wbw-result.html` and `orphaned-words-result.html` in the `result/DCP directory`, type the following command:


```
ant run-dcp
```

The commands to run only the Document Comparator DCP sample code are the same as for the DXP sample above, but the 'dcp-wbw' configuration-id is used instead of 'wbw', for example, replacing x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`


```
java -jar ../../deltaxml-x.y.z.jar compare dcp-wbw input1.xml input2.xml non-wbw-result.html
```

If you wish to see the xml delta file result rather than an html result for the Pipelined Comparator samples, simply add the parameter `convert-to-html=false` to the end of any of the commands.

## Document Comparator (Java API)
To run just the Document Comparator API sample and produce the output files `non-wbw-result.html`, `wbw-result.html` and `orphaned-words-result.html` in the `result/DocumentComparator` directory, type the following command:


```
ant run-dc
```

The commands to compile and run only the Document Comparator sample code are as follows. Replace x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`


```
mkdir bin
javac -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar -d bin ./src/java/com/deltaxml/samples/DocumentComparatorSample.java
java -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.DocumentComparatorSample
```

If you wish to see the XML delta file result rather than an HTML result for the Comparator samples above, simply add the parameter `convert-to-html=false` to the end of any of the commands.

To clean up the sample directory, run the following Ant command.


```
ant clean
```
